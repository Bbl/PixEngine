#include "UIButton.h"
#include <SFML/Graphics/Texture.hpp>
#include "EnvironmentVariables.h"


UIButton::UIButton(sf::Texture& texture) 
	: UIComponent(texture)
{
}


UIButton::~UIButton()
{

}

void UIButton::render()
{
	UIComponent::render();
}