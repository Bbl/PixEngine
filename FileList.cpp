#include "FileList.h"
#include "Entities.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>

namespace fs = boost::filesystem;

std::vector<std::string> FileList::textureFileTypes;
std::vector<std::string> FileList::fontFileTypes;
std::vector<std::string> FileList::soundFileTypes;

/*
	File types.
*/
FileList::FileList()
{

}

FileList::~FileList()
{

}
/*
	Public methods
*/

void FileList::save(path directoryLocation)
{
	initializeFileTypes();
	addEntities(directoryLocation);
	saveVector(textureList, "assets/data/textures.txt");
	saveVector(fontList, "assets/data/fonts.txt");
	saveVector(soundList, "assets/data/sounds.txt");
	std::cout << "[ENGINE] Done with saving files into txt" << std::endl;
	remove();
}

void FileList::load()
{
	loadVector(textureList, "assets/data/textures.txt");
	loadVector(fontList, "assets/data/fonts.txt");
	loadVector(soundList, "assets/data/sounds.txt");
	std::cout << "[ENGINE] Done with loading List files." << std::endl;
}

void FileList::remove()
{
	Entities::vectorCleaing(fontList);
	Entities::vectorCleaing(textureList);
	Entities::vectorCleaing(soundList);
	textureFileTypes.clear();
	fontFileTypes.clear();
	soundFileTypes.clear();
}

/*
	_________DATA ACCESS METHODS_________
*/

std::string FileList::getFileLocation(FileTypes type, std::string fileName)
{
	std::vector<FileDetails*> *dataVector = nullptr;
	if (type == FileTypes::Texture)
		dataVector = &textureList;
	else if (type == FileTypes::Font)
		dataVector = &fontList;
	else if (type == FileTypes::Sound)
		dataVector = &soundList;
	
	for (int x = 0; x < dataVector->size(); x++)
	{
		if (dataVector->at(x)->fileName.compare(fileName))
		{
			return dataVector->at(x)->fileLocation;
		}
	}
	return 0;
}

/*
	Private methods
*/


/*
	_________SAVE METHODS_________
*/
void FileList::initializeFileTypes()
{
	if (!initializedTypes)
	{
		textureFileTypes.push_back(".png");
		fontFileTypes.push_back(".ttf");
		soundFileTypes.push_back(".mp3");
		initializedTypes = true;
	} 
}

void FileList::addEntities(path directory)
{
	if (exists(directory)) 
	{
		for (directory_entry& entry : directory_iterator(directory))
		{
			if (is_regular_file(entry))
			{
				determineFileType(textureFileTypes, textureList, entry);
				determineFileType(fontFileTypes, fontList, entry);
				determineFileType(soundFileTypes, soundList, entry);
			}
			if (is_directory(entry))
			{
				addEntities(entry);
			}
		}
	}
}

void FileList::determineFileType(std::vector<std::string> &vector, std::vector<FileDetails*> &list, directory_entry& entry)
{
	std::string fileName = entry.path().filename().string();
	std::string extension = entry.path().extension().string();
	auto result = std::find(vector.begin(), vector.end(), extension);
	
	if (result != vector.end())
	{
		//std::cout << "Found a matching extension" << entry.path().extension() << std::endl;
		fileName.erase(fileName.length() - extension.length(), fileName.length());
		list.push_back(new FileDetails(fileName, entry.path().string()));
	}
}

void FileList::saveVector(std::vector<FileDetails*> &vector, std::string saveLocation)
{
	std::ofstream outputFile(saveLocation);
	for (int x = 0; x != vector.size(); x++)
	{
		outputFile << vector[x]->fileName << '-' << vector[x]->fileLocation << std::endl;
	}
	outputFile.close();
}


/*
	_________LOAD_________
*/
void FileList::loadVector(std::vector<FileDetails*> &vector, std::string txtLocation)
{
	std::ifstream file(txtLocation);
	std::string line;

	std::string fileName;
	std::string fileLocation;
	
	if (file)
	{
		while (std::getline(file, line))
		{
			fileName = line.substr(0, line.find("-") - 1);
			fileLocation = line.substr(line.find("-") + 1, line.length());
			std::cout << line << std::endl;
			vector.push_back(new FileDetails(fileName, fileLocation));
		}
	}
	else
	{
		std::cout << "[FileList]Can't find file: " << txtLocation << std::endl;
	}
}