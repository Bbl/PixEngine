#pragma once
#include "UIComponent.h"

class UIButton : public UIComponent
{
public:

	UIButton(sf::Texture& texture);

	~UIButton();

	void render();

};

