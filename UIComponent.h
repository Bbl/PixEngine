#pragma once
#include "Clickable.h"
#include <iostream>


class UIComponent : public sf::Sprite, public Clickable
{
	friend class Skin;
protected:

	UIComponent(sf::Texture& texture);

	virtual ~UIComponent();

public:

	virtual void render();

	virtual void update(); 

};

