#pragma warning (disable : 4996)
#include "UIComponent.h"
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

class UITextInput : public UIComponent
{
public:

	UITextInput( sf::Texture& texture, sf::Font& font, int size);

	~UITextInput();

	void render();

	void update();

private:

	std::string text = "jkds";

	bool active = false;

	void isActive();

	void addCharacterToText();

	sf::Text text1;
};

