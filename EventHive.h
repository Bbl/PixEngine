#pragma once
#include <SFML/Window/Event.hpp>
/*
	EventHive is still a pre-alpha concept of handling events.
	I haven't done much thinking about it but it might be good.
	Every class that inherits EventProcessor can check if keyboard was clicked etc.
	Check class code for further details.
*/
class EventHive : public sf::Event
{
	friend class EventProcessor;
public:
	//This function have to be called in a main loop
	void hive();
	//Gets event
	static EventHive& getInstance();
	sf::Event& getEvent();
private:
	EventHive();
	~EventHive();
	//Singleton
	static EventHive *instance;

	//Don't so sure about it yet, could be a struct or union.Will find out later.
	bool static isMouseButtonPressed;
	bool static isMouseButtonRelased;
	bool static isKeyClicked;
	//This function sets every variable to false.Just to reset them on a start of a hive()
	void clear();
};

