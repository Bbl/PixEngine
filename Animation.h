#pragma once
#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\Rect.hpp>
#include <vector>
#include <map>

class Animation : public sf::Sprite
{
public:
	Animation(std::string name); // Default constuctor.
	/*
		Methods
	*/
	void play();
	void stop();
	void pause();
	void resume();
	void changeFrame(std::string frameName);
	void update(); //ITS IMPORTANT TO CALL THAT OTHERWISE ANIMATION WON'T PLAY.
private:
	/*
		Data structs.
	*/
	struct Frame {
		sf::IntRect region;
		float time;
		int frameNumber;
		Frame(sf::IntRect region, float time, int frameNumber)  : region(region), time(time),
			frameNumber(frameNumber){}
	};

	/*
		Fields
	*/
	std::map<std::string, sf::IntRect*> frames; // Frames are added later to this.

	/*
		Methods
	*/
};
