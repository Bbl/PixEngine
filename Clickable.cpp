#include "Clickable.h"
#include "EnvironmentVariables.h"
#include <SFML/Window/Mouse.hpp>
#include <iostream>

Clickable::Clickable()
{
}


Clickable::~Clickable()
{
}

bool Clickable::isClicked(sf::Sprite sprite)
{
	sf::Vector2f ObjectPosition = sprite.getPosition();
	sf::Vector2u ObjectSize(sprite.getTextureRect().width, sprite.getTextureRect().height);
	
	if (wasMouseButtonDown())
	{
		sf::Vector2i position = sf::Mouse::getPosition(EnvironmentVariables::getVariables().getWindow());
		if (position.x <= ObjectPosition.x + ObjectSize.x && position.x > ObjectPosition.x
			&& position.y <= ObjectPosition.y + ObjectSize.y && position.y > ObjectPosition.y)
		{
			return true;
		}
	}
	return false;
}