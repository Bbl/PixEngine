#pragma once
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include "EventHive.h"
#include <iostream>

class EnvironmentVariables
{
public:

	static EnvironmentVariables& getVariables();

	static void setWindowSize(int windowWidth, int windowHeight);

	static void setWindowName(std::string windowName);

	sf::RenderWindow& getWindow();

	sf::RenderTexture& getRenderTexture();

	sf::Event& getEvent();

	EventHive& getEventHive();
protected:

	EnvironmentVariables();

	~EnvironmentVariables();

private:
	static EnvironmentVariables *instance;

	static int windowWidth, windowHeight;

	static std::string windowName;

	//Variables
	sf::RenderWindow window;
	//sf::Event event; Out of use because of EventHive, still in progress at this moment.
	sf::RenderTexture renderTexture;
	//Polling events
	EventHive *eventHive;
};

