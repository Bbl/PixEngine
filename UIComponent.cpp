#include "UIComponent.h"
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "EnvironmentVariables.h"
#include <iostream>
using namespace std;


UIComponent::UIComponent(sf::Texture& texture)
{
	sf::Sprite::setTexture(texture);
}


UIComponent::~UIComponent()
{
}
 
void UIComponent::render()
{
	update();
	EnvironmentVariables::getVariables().getWindow().draw(*this);
}

void UIComponent::update()
{
}