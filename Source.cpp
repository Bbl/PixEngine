#include <SFML/Graphics.hpp>
#include <cmath>
#include "UIButton.h"
#include "Skin.h"
#include <iostream>
#include "EnvironmentVariables.h"
#include "UITextInput.h"
#include "FileList.h"

int main()
{
	EnvironmentVariables::setWindowSize(1280, 720);
	EnvironmentVariables::setWindowName("3 godziny roboty");
	EnvironmentVariables &variables = EnvironmentVariables::getVariables();
	sf::Texture texture, texture1;
	texture.loadFromFile("assets\\button_x.png", sf::IntRect(0, 0, 128, 128));
	texture1.loadFromFile("assets\\textinput.png", sf::IntRect(0, 0, 256, 80));
	UIButton button(texture); 
	button.move(1152, 0);
	sf::Font font;
	// Load it from a file
	font.loadFromFile("assets\\arial.ttf");
	UITextInput text(texture1, font, 40);
	FileList list;
	//list.save("assets/");
	list.load();
	std::cout << list.getFileLocation(FileList::Font, "arial") << std::endl;
	while (variables.getWindow().isOpen())
	{
		variables.getEventHive().hive();
		variables.getWindow().clear();
		button.render();
		text.render();
		variables.getWindow().display();
		
		if (button.isClicked(button))
		{
			variables.getWindow().close();
		}
	}
	return 0;
}