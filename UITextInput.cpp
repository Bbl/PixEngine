#include "UITextInput.h"
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include "EnvironmentVariables.h"

UITextInput::UITextInput(sf::Texture& texture, sf::Font& font, int size)
	: UIComponent(texture)
{
	text1.setString(text);
	text1.setFont(font);
	text1.setCharacterSize(size);
	text1.setStyle(sf::Text::Bold);
	text1.setColor(sf::Color::Black);
	text1.setPosition(getPosition());
	text1.move(50, 14);

}


UITextInput::~UITextInput()
{
}

void UITextInput::render()
{
	UIComponent::render();
	EnvironmentVariables::getVariables().getWindow().draw(text1);
}

void UITextInput::update()
{
	text1.setString(text);
	isActive();
	if(active)
		addCharacterToText();

}

void UITextInput::isActive()
{
	if (wasMouseButtonDown())
	{
		if (isClicked(*this) && !active)
		{
			active = true;
			std::cout << "Set to active" << std::endl;
		}
		else if(active && !isClicked(*this))
		{
			active = false;
			std::cout << "Set to inactive" << std::endl;
		}
	}
}

void UITextInput::addCharacterToText()
{
	if (wasKeyboardKeyClicked())
	{

	}
}
