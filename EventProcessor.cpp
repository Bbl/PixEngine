#include "EventProcessor.h"
#include "EventHive.h"


EventProcessor::EventProcessor()
{
}


EventProcessor::~EventProcessor()
{
}

bool EventProcessor::wasMouseButtonDown()
{
	return EventHive::isMouseButtonPressed;
}

bool EventProcessor::wasMouseButtonRelased()
{
	return EventHive::isMouseButtonRelased;
}

bool EventProcessor::wasKeyboardKeyClicked()
{
	return EventHive::isKeyClicked;
}

sf::Event EventProcessor::getEvent()
{
	return EventHive::getInstance().getEvent();
}