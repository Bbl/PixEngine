#pragma once
#include <iostream>
#include <vector>
#include <boost/filesystem.hpp>
#include <string>

using namespace boost::filesystem;

/*
	FileList is a class that provides easy access to files in your game.
	Can be use in both ways to save files or load them.
	Save should be called when ever you add new files into your "assets" folder and should't be left in relase.
	It might slow down loading of a game, so make sure to remove save from your relase.
	Accesing to data can only be made by calling load() first otherwise nothing will work.
*/

class FileList
{
public:
	FileList();
	~FileList();
	void save(path directoryLocation);
	void load();
	void remove();

	enum FileTypes {
		Texture, Font, Sound
	};

	/*
		_________Data access methods_________
	*/
	std::string getFileLocation(FileTypes type, std::string fileName);
private:
	/*
		At this moment FileList supports only two fields.
		Might add more in future but right now it's not that important.
	*/
	struct FileDetails
	{
	public:
		FileDetails(std::string fileName, std::string fileLocation) : fileName(fileName), fileLocation(fileLocation)
		{}
		std::string fileName;
		std::string fileLocation;
	};

	/*
		_________SAVING METHODS_________
	*/

	//Fields containing data.
	std::vector<FileDetails*> fontList;
	std::vector<FileDetails*> textureList;
	std::vector<FileDetails*> soundList;

	//Initialization of supported types of files.
	void initializeFileTypes();
	//Adds everyfile that matches with predefined type.
	void addEntities(path directory);
	//Determines file type and adds it to its vector.
	void determineFileType(std::vector<std::string> &vector, std::vector<FileDetails*> &list, directory_entry& entry);
	//Saves vector data into txt files.
	void saveVector(std::vector<FileDetails*> &vector, std::string saveLocation);

	/*
		_________LOADING METHODS_________
	*/
	
	//Load data into vectors.
	void loadVector(std::vector<FileDetails*> &vector, std::string txtLocation);

	/* 
		_________Types of files_________
	*/
	static std::vector<std::string> textureFileTypes;
	static std::vector<std::string> fontFileTypes;
	static std::vector<std::string> soundFileTypes;
	bool initializedTypes = false;
};