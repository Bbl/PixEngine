#include "EventHive.h"
#include "EnvironmentVariables.h"

EventHive *EventHive::instance;
bool EventHive::isMouseButtonPressed;
bool EventHive::isMouseButtonRelased;
bool EventHive::isKeyClicked;

EventHive::EventHive()
{
}


EventHive::~EventHive()
{
}

/*
	Put it in main loop.
*/
void EventHive::hive()
{
	clear();
	while (EnvironmentVariables::getVariables().getWindow().pollEvent(*this)) {
		switch (type)
		{
			case sf::Event::Closed:
				EnvironmentVariables::getVariables().getWindow().close();
			break;
			case sf::Event::MouseButtonPressed:
				isMouseButtonPressed = true;
			break;
			case sf::Event::MouseButtonReleased:
				isMouseButtonRelased = true;
			break;
			case sf::Event::KeyPressed:
				isKeyClicked = true;
			break;
		}
	}
}

void EventHive::clear()
{
	isMouseButtonPressed = false;
	isMouseButtonRelased = false;
	isKeyClicked = false;
}

//Singleton.
EventHive& EventHive::getInstance()
{
	if (instance == nullptr)
	{
		instance = new EventHive();
	}
	return *instance;
}

sf::Event& EventHive::getEvent()
{
	return *this;
}