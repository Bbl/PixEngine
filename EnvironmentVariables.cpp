#include "EnvironmentVariables.h"
#include "EventHive.h"
#include <SFML/Graphics/RenderTexture.hpp>
#include <iostream>

int EnvironmentVariables::windowHeight;
int EnvironmentVariables::windowWidth;
std::string  EnvironmentVariables::windowName;
EnvironmentVariables *EnvironmentVariables::instance;

EnvironmentVariables::EnvironmentVariables()
	: renderTexture(),
	window(sf::VideoMode(windowWidth, windowHeight), windowName)
{	
	eventHive = &EventHive::getInstance();
	renderTexture.create(windowWidth, windowHeight);
}


EnvironmentVariables::~EnvironmentVariables()
{

}

EnvironmentVariables& EnvironmentVariables::getVariables()
{
	if (instance == nullptr)
		instance = new EnvironmentVariables();

	return *instance;
}

void EnvironmentVariables::setWindowSize(int windowWidth, int windowHeight)
{
	EnvironmentVariables::windowHeight = windowHeight;
	EnvironmentVariables::windowWidth = windowWidth;
}

void EnvironmentVariables::setWindowName(std::string windowName)
{
	EnvironmentVariables::windowName = windowName;
}

sf::RenderWindow& EnvironmentVariables::getWindow()
{
	return window;
}

sf::RenderTexture& EnvironmentVariables::getRenderTexture()
{
	return renderTexture;
}

sf::Event& EnvironmentVariables::getEvent()
{
	return eventHive->getEvent();
}

EventHive& EnvironmentVariables::getEventHive()
{
	return *eventHive;
}