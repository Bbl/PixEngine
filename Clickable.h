#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include "EventProcessor.h"

class Clickable : public EventProcessor
{
public:

	Clickable();

	virtual ~Clickable();

	bool isClicked(sf::Sprite sprite);
};

