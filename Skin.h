#pragma once
#include "JSONLoader.h"
#include "UIComponent.h"

class Skin
{
public:
	Skin(UIComponent component);
	~Skin();
private:
	JSONLoader *jsonLoader;
};