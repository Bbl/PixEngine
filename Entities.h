#pragma once
#include <vector>

class Entities {
public:
	
	template <typename T>
	static void vectorCleaing(std::vector<T*> vector)
	{
		for (std::vector<T*>::iterator it = vector.begin(); it != vector.end(); ++it)
		{
			delete(*it);
		}
		vector.clear();
	}
};