#pragma once
#include "EventHive.h"

class EventProcessor
{
protected:
	EventProcessor();
	~EventProcessor();
	bool static wasMouseButtonDown();
	bool static wasMouseButtonRelased();
	bool static wasKeyboardKeyClicked();
	sf::Event getEvent();
};

